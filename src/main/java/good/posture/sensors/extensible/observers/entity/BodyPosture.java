package good.posture.sensors.extensible.observers.entity;

public class BodyPosture {

	private String bodyPart;
	
	private Integer value;

	public BodyPosture(String bodyPart, Integer value) {
		this.bodyPart = bodyPart;
		this.value = value;
	}
	public String getBodyPart() {
		return bodyPart;
	}

	public Integer getValue() {
		return value;
	}
}
