package good.posture.sensors.extensible.observers;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import good.posture.sensors.extensible.observers.entity.BodyPosture;

@SuppressWarnings("deprecation")
public class BodyPostureObserver implements Observer {

	private List<BodyPosture> postures;
	
	public BodyPostureObserver() {
		this.postures = new ArrayList<BodyPosture>();
	}

	@Override
	public void update(Observable o, Object arg) {
		postures.add((BodyPosture) arg);
		this.postures.forEach(posture -> {
			System.out.println("[" + posture.getBodyPart() + ": " + posture.getValue() + " ] ");
		});
	}

}
